#!/usr/bin/python3
import argparse
import shutil
import re
import git
import gitlab
from gitlab.v4.objects import ProjectMergeRequest
import os

# Global Variables
src_repo_dir = os.path.abspath('SRC_Repository')
dst_repo_dir = os.path.abspath('DST_Repository')
dst_repo_branch = 'changesfromext'  # branch in internal project used to collect changes


def import_repo(repo_url, repo_dir):
    """Clone Repositories"""
    repo = git.Repo.clone_from(repo_url, repo_dir)
    return repo


def check_branch(dst_repo_git):
    """Check if Merge Branch exists"""
    remote_branch = dst_repo_git.remote().refs
    for branch in remote_branch:
        if branch.name == f'origin/{dst_repo_branch}':
            dst_repo_git.git.checkout(dst_repo_branch)
            return True
    return False


def create_branch(project_id, dst_repo_git, gitlab_token):
    """Create Branch if it doesn't exist"""
    gl = gitlab.Gitlab('https://gitlab.rrze.fau.de', private_token=gitlab_token)
    project = gl.projects.get(project_id)
    project.branches.create({'branch': dst_repo_branch,
                             'ref': dst_repo_git.heads[dst_repo_git.active_branch.name].tracking_branch().remote_head})
    dst_repo_git.git.checkout(dst_repo_branch)


def merge_requests(gitlab_token, project_id, src_repo_name):
    """Open Merge Request if it isn't already open"""
    gl = gitlab.Gitlab('https://gitlab.rrze.fau.de', private_token=gitlab_token)
    project = gl.projects.get(project_id)
    mr_title = f'Merging from {src_repo_name}'
    mr_title_exists = False
    open_mr = project.mergerequests.list(state='opened')
    for mr in open_mr:
        if mr.title == mr_title:
            mr_title_exists = True
    if not mr_title_exists:
        try:
            project.mergerequests.create({'source_branch': dst_repo_branch,
                                          'target_branch': 'main',
                                          'title': mr_title})
        except Exception as e:
            exit(e)


def patch_changes(dst_repo_git, changes):
    """Find Changes inside the Repositories"""
    exclude_files = [".gitlab-ci.yml", "mirror_sofa_repo.py",
                     "/web/.vitepress/config.mts", ".git", ".gitignore"]
    exclude_args = ' '.join([f'--exclude={file}' for file in exclude_files])
    os.system(f'rsync -r -av --delete {exclude_args} {src_repo_dir}/ {dst_repo_dir}/')
    # DO NOT TOUCH REPLACEMENTS, UNLESS YOU KNOW WHAT TO DO!! (N.H)
    url_replacements = {
        # URL Replacements (FeedInfo.vue, tool-scripts,)
        "https://sofa.macadmins.io/": "https://faumac.pages.rrze.fau.de/sofa/",
        "https://sofafeed.macadmins.io/v1/": "https://faumac.pages.rrze.fau.de/sofa/assets/",
        # CNAME Replacement
        "sofa.macadmins.io": "faumac.pages.rrze.fau.de",
        # Landing Page replacements (index.md)
        "src: /custom_logo.png": "src: assets/custom_logo.png",
        "src=\"images/custom_logo.png\"": "src=\"assets/custom_logo.png\"",
        "href=\"images/custom_logo.png\"": "href=\"assets/custom_logo.png\"",
        "content=\"SOFA - a simple, organised feed of macOS and iOS update info\"": "content=\"A simple, organised feed of macOS and iOS update info\"",
        "content=\"SOFA supports MacAdmins by efficiently tracking and surfacing information on updates for macOS and iOS.\"": "content=\"A simple, organised feed of macOS and iOS update info\"",
        "name: \"SOFA - Simple Organized Feed for Apple Software Updates\"": "name: \"FAUmac SOFA\"",
        "meta property=\"og:title\" content=\"A simple, organised feed of macOS and iOS update info\"": "meta property=\"og:title\" content=\"FAUmac SOFA\"",
        "tagline: SOFA supports MacAdmins by efficiently tracking and surfacing information on updates for macOS and iOS.": "tagline: Simple Organized Feed for Apple Software Updates",
        # Site Redirections corrections (index.md, SecurityInfo.vue)
        "\"/release-deferrals.html\"": "\"./release-deferrals.html\"",
        ":href=\"`/cve-details.html?cveId=${cve}`\"": ":href=\"`./cve-details.html?cveId=${cve}`\"",
        "'Sequoia 15': '/macOS_Sequoia.html',": "'Sequoia 15': './macOS_Sequoia.html',",
        "'Sonoma 14': '/macOS_Sonoma.html',": "'Sonoma 14': './macOS_Sonoma.html',",
        "'Ventura 13': '/macOS_Ventura.html',": "'Ventura 13': './macOS_Ventura.html',",
        "'Monterey 12': '/macOS_Monterey.html',": "'Monterey 12': './macOS_Monterey.html',",
    }
    relevant_extensions = [".html", ".sh", ".json", ".gs", ".vue", ".md", "CNAME"]

    for subdir, dirs, files in os.walk(dst_repo_dir):
        for file in files:
            if any(file.endswith(ext) for ext in relevant_extensions):
                file_path = os.path.join(subdir, file)
                with open(file_path, 'r') as f:
                    content = f.read()
                for old_url, new_url in url_replacements.items():
                    content = re.sub(re.escape(old_url), new_url, content)
                with open(file_path, 'w') as f:
                    f.write(content)

                print(f"Changed URLs in {file_path}.")

    if dst_repo_git.untracked_files or dst_repo_git.is_dirty():
        dst_repo_git.git.add('--all')
        dst_repo_git.index.commit(changes)
        dst_repo_git.git.push('origin', dst_repo_branch)
        return True
    else:
        print('Nothing to commit!')


def cleaning():
    """Removing Directories after Work is done"""
    try:
        shutil.rmtree(src_repo_dir)
        shutil.rmtree(dst_repo_dir)
        return 0
    except Exception as e:
        return e


def main():
    parser = argparse.ArgumentParser(
        prog='Repository Mirror Tool',
        description='Mirrors Repositories from Developer to our own Repository',
        epilog='Questions/Problems? -> rrze-mac@fau.de')

    parser.add_argument('-p', '--project',
                        dest='src_repo_name',
                        type=str,
                        help='name of the repository you want to update'
                        )
    parser.add_argument('-d', '--destination',
                        dest='dst_repo_name',
                        type=str,
                        help='name of the destination repository'
                        )

    parser.add_argument('-i', '--project-id',
                        dest='project_id',
                        type=str,
                        help='Project ID to the internal project'
                        )

    parser.add_argument('-t', '--gitlab-group-token',
                        dest='gitlab_group_token',
                        type=str,
                        help='Runners token'
                        )
    '''ARGPARSE Arguments'''
    args = parser.parse_args()
    gitlab_token = args.gitlab_group_token
    src_repo_name = args.src_repo_name
    dst_repo_name = args.dst_repo_name
    project_id = args.project_id
    '''Repository URLs'''
    src_repo_url = f'https://github.com/macadmins/{src_repo_name}'  # GitHub SOFA
    dst_repo_url = f'https://gitlab-ci-token:{gitlab_token}@gitlab.rrze.fau.de/faumac/{dst_repo_name}'  # GitLab SOFA
    try:
        import_repo(src_repo_url, src_repo_dir)
        dst_repo_git = import_repo(dst_repo_url, dst_repo_dir)
        branch_exists = check_branch(dst_repo_git)
        if not branch_exists:
            create_branch(project_id, dst_repo_git, gitlab_token)
        done_changes = patch_changes(dst_repo_git, changes=f'Merging from {src_repo_name}')
        if done_changes:
            merge_requests(gitlab_token, project_id, src_repo_name)
    finally:
        cleaning()


main()
